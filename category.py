# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, fields
from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond.wizard import Wizard, StateAction, Button, StateView


class CategoryOpenPartyStart(ModelView):
    """Party category Open party start"""
    __name__ = 'party.category.open_party.start'

    mode = fields.Selection([
        ('all', 'All'),
        ('any', 'Any')], 'Mode', required=True)
    categories = fields.Many2Many('party.category', None, None,
        'Categories', required=True)


class CategoryOpenParty(Wizard):
    """Party category Open party"""
    __name__ = 'party.category.open_party'

    start = StateView('party.category.open_party.start',
        'party_category_form_relate.open_party_start_view_form',
        [Button('Cancel', 'end', 'tryton-cancel'),
         Button('OK', 'open_', 'tryton-ok', default=True)])
    open_ = StateAction('party.act_party_form')

    def default_start(self, fields):
        return {
            'mode': 'all',
            'categories': Transaction().context['active_ids']}

    def do_open_(self, action):
        pool = Pool()
        Party = pool.get('party.party')

        _domain = ['AND' if self.start.mode == 'all' else 'OR', ]
        cat_ids = [c.id for c in self.start.categories]
        for cat_id in cat_ids:
            _domain.append(('categories', '=', cat_id))
        parties = Party.search(_domain)
        return action, {'res_id': list(map(int, parties))}
