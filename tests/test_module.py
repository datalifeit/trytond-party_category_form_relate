# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.tests.test_tryton import ModuleTestCase


class PartyCategoryFormRelateTestCase(ModuleTestCase):
    """Test Party Category Form Relate module"""
    module = 'party_category_form_relate'


del ModuleTestCase
